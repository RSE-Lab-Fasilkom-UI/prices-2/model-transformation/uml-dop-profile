# UML-DOP profile

## Intoroduction
A UML profile is a mechanism to extend UML metamodel for specific purposes. 
The UML-DOP profile is a UML profile that is defined based on delta-oriented programming (DOP).
The UML-DOP profile supports UML modeling for software product line engineering.
Some stereotypes are defined to extend existing metaclasses in UML. 

First, the UML profile is defined based on 
DOP language, abstract behavioral specification (ABS) in [1], called UML-ABS profile.
The UML-ABS profile was defined specific to ABS language without considering
DOP in general. Then, the UML-ABS profile is revised to a UML profile for
delta-oriented programming (UML-DOP) [2]
The UML-DOP profile was extended to support multi-product lines [3], 
variability modules [4], 
and microservice-based product lines [5].

## How to use the profile? 
The UML-DOP profile in this repository is defined with Papyrus Eclipse.
- Install an Eclipse Modling Tools and Papyrus Eclipse Plugin.
- Import the UML-DOP profile to your project.
- Create a new papyrus project to create a new UML diagram. In the 
project creation wizard, choose profiles to apply by browsing 
the imported uml-dop project.

## References
[1] Setyautami, M. R. A. (2013). Applying UML profile and refactoring rules to create software product line from UML class diagram (Master Thesis). Fakultas Ilmu Komputer Universitas Indonesia. <br />

[2] Setyautami, M. R. A., Hahnle, R., Muschevici, R., & Azurat, A. (2016). A UML profile for delta-oriented programming to support software product line engineering. In Proceedings of the 20th international systems and software product line conference (p. 45–49). New York, NY, USA: Association for Computing Machinery. [doi: 10.1145/ 2934466.2934479](https://dl.acm.org/doi/10.1145/2934466.2934479). <br />

[3] Setyautami, M. R. A., Adianto, D., & Azurat, A. (2018). Modeling multi software product lines using UML. In Proceedings of the 22nd international systems and software product line conference - volume 1 (p. 274–278). New York, NY, USA: Association for Computing Machinery. [doi: 10.1145/3233027.3236400](https://dl.acm.org/doi/10.1145/3233027.3236400). <br />

[4] Setyautami, M. R. A., & Hahnle, R. (2021). An architectural pattern to realize multi software product lines in java. In 15th international working conference on variability modelling of software-intensive systems. New York, NY, USA: Association for Computing Machinery. [doi: 10.1145/3442391.3442401](https://dl.acm.org/doi/10.1145/3442391.3442401). <br />

[5] Setyautami, M. R. A., Fadhlillah, H. S., Adianto, D., Affan, I., & Azurat, A. (2020). Variability management: Re-engineering microservices with delta-oriented software product lines. In Proceedings of the 24th acm conference on systems and software product line: Volume a - volume a. New York, NY, USA: Association for Computing Machinery. [doi: 10.1145/3382025.3414981](https://dl.acm.org/doi/10.1145/3382025.3414981). 
